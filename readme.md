## Mi primera API con express + ODM

Creando una API en Express.js con Mongoose como (ODM) y empleando Railway para instanciar la base de datos.

## Requisitos

- Node.js
- NPM
- Express.js

## Instalación 

Para instalar lo en tu computadora, puedes lonar el repositorio así:

- git clone git@gitlab.com:hi7114906/mi-primera-api-con-express-odm.git

## Uso 

Puedes ver el funcionamiento de la aplicación ya ejecutada en el este link:

- https://videoclubodm-jd1617f8.b4a.run/ 

## Licencia 

- Zaid Joel González Mendoza (353254)
- Web Platforms
- 7CC2


